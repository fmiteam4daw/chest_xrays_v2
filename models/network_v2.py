import torch
import torch.nn as nn
import pdb

class NetworkV2(nn.Module):
    def __init__(self, base, num_classes):
        super().__init__()
        self.base = base
        self.gradients = None

        self.linear1 = nn.Linear(1024, 64)
        self.linear2 = nn.Linear(64, 1024)

        if hasattr(base, 'fc'):
            in_features = self.base.fc.in_features
            self.base.fc = nn.Sequential(nn.Linear(in_features, num_classes))
                                        #  nn.Softmax())

        elif isinstance(base.classifier, nn.Linear): # densenet121
            in_features = self.base.classifier.in_features
            self.base.classifier = nn.Sequential(
                                        nn.Linear(in_features, num_classes),
                                        nn.Softmax())

        else: # mobilenetv2 / vgg19
            in_features = self.base.classifier[-1].in_features
            self.base.classifier[-1] = nn.Linear(in_features=in_features, out_features=num_classes, bias=True)

    def se_block(self, in_block):
        ch = in_block.shape[1]
        height = in_block.shape[-2]

        x = nn.AvgPool2d(height)(in_block)
        x = nn.Flatten()(x)
        x = self.linear1(x)
        x = nn.ReLU()(x)
        x = self.linear2(x)
        x = nn.Sigmoid()(x)
        x = x.view(-1, ch, 1, 1)
        return torch.mul(in_block, x)

    def forward(self, x):
        if hasattr(self.base, 'fc'):
            all_layers = list(self.base.children())
            head = nn.Sequential(*all_layers[:-3])
            tail = nn.Sequential(*all_layers[-3:-1], nn.Flatten())

            x = head(x)
            x = self.se_block(x)

            x = tail(x)
            x = self.base.fc(x)

        elif isinstance(self.base.classifier, nn.Linear): # densenet121
            all_layers = list(self.base.children())
            conv_layers = list(all_layers[0])

            head = nn.Sequential(*conv_layers[:-3])
            tail = nn.Sequential(*conv_layers[-3:])

            x = head(x)
            x = self.se_block(x)
            x = tail(x)
            
            x = nn.AvgPool2d(x.shape[-2])(x)
            x = nn.Flatten()(x)
            x = all_layers[1](x)
        else: # mobilenetv2 / vgg19
            x = self.base(x)

        return x