import torch.nn as nn
import pdb

class NetworkV3Cam(nn.Module):
    def __init__(self, base, num_classes):
        super().__init__()
        self.base = base
        self.gradients = None

        if hasattr(base, 'fc'):
            in_features = self.base.fc.in_features

            self.base.fc = nn.Sequential(
                                        nn.Dropout(),
                                        nn.Linear(in_features=in_features, out_features=512),
                                        nn.ReLU(),
                                        nn.Dropout(),
                                        nn.Linear(in_features=512, out_features=256),
                                        nn.ReLU(),
                                        nn.Dropout(),
                                        nn.Linear(in_features=256, out_features=num_classes))
        elif isinstance(base.classifier, nn.Linear): # densenet121
            in_features = self.base.classifier.in_features
            self.base.classifier = nn.Sequential(
                                        nn.Linear(in_features, num_classes),
                                        nn.Sigmoid())
        else: # mobilenetv2 / vgg19
            in_features = self.base.classifier[-1].in_features
            self.base.classifier[-1] = nn.Linear(in_features=in_features, out_features=num_classes, bias=True)

    def activations_hook(self, grad):
        self.gradients = grad

    # method for the gradient extraction
    def get_activations_gradient(self):
        return self.gradients
    
    # method for the activation exctraction
    def get_activations(self, x):
        layers = list(self.base.children())
        head = nn.Sequential(*layers[:-2])
        return head(x)

    def forward(self, x):
        if hasattr(self.base, 'fc'):
            layers = list(self.base.children())
            head = nn.Sequential(*layers[:-2])
            tail = nn.Sequential(layers[-2], nn.Flatten(), layers[-1])

            x = head(x)
            h = x.register_hook(self.activations_hook)
            fc = tail(x)
        else:
            fc = self.base(x)
        return fc