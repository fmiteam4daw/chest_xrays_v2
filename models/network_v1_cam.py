import torch.nn as nn
import pdb

class NetworkV1Cam(nn.Module):
    def __init__(self, base, num_classes):
        super().__init__()
        self.base = base
        self.is_mobile = False

        if hasattr(base, 'fc'):
            in_features = self.base.fc.in_features
            self.base.fc = nn.Linear(in_features, num_classes)
        else: 
            self.is_mobile = True # mobile net v2
            in_features = self.base.last_channel

            self.base.classifier = nn.Sequential(
                nn.Dropout(0.2),
                nn.Linear(in_features, num_classes),
            )

        # placeholder for the gradients
        self.gradients = None

    def activations_hook(self, grad):
        self.gradients = grad

    # method for the gradient extraction
    def get_activations_gradient(self):
        return self.gradients
    
    # method for the activation exctraction
    def get_activations(self, x):
        layers = list(self.base.children())
        head = nn.Sequential(*layers[:-2])
        return head(x)

    def forward(self, x):
        layers = list(self.base.children())
        head = nn.Sequential(*layers[:-2])
        tail = nn.Sequential(layers[-2], nn.Flatten(), layers[-1])

        x = head(x)

        # register the hook
        h = x.register_hook(self.activations_hook)

        x = tail(x)
        return x