#### train
python train.py [--arch densenet121] [--finetune --path ../logs/densenet121/1/best.pth] [--epochs 10] [--batch-size 14]

#### tensorboard
tensorboard --logdir=runs/chest_xray/20200312_191546