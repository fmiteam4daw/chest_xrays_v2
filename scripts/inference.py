import argparse
import json
import os
import time
import io
import pdb
import cv2

import ntpath
import pandas as pd
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F

from torchvision import transforms
from torchvision.transforms.functional import to_tensor, normalize
from dataset import load_class_names, prepare_loader
from PIL import Image

import sys
sys.path.insert(0, os.path.join('../'))
from models import construct_model

def load_weight(model, path):
    sd = torch.load(path)
    model.load_state_dict(sd['model'])

    return model

def transform_image(image_bytes, img_size):
    this_transforms = transforms.Compose([
                    transforms.Resize(size=256),
                    transforms.CenterCrop(size=img_size),
                    transforms.ToTensor(),
                    transforms.Normalize(
                        [0.485, 0.456, 0.406],
                        [0.229, 0.224, 0.225]
                    )
                ])

    image = Image.open(io.BytesIO(image_bytes)).convert('RGB')
    return this_transforms(image).unsqueeze(0)

def main(args):
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    torch.cuda.empty_cache()

    config = {
        'image_size': (args.imgsize, args.imgsize),
        'arch': args.arch,
        'model_path': args.model_path,
        'image_path': args.image_path,
        'version': '3CAM'
    }

    class_names = load_class_names()
    num_classes = len(class_names)

    # load model
    model = construct_model(config, num_classes)
    model = model.to(device)
    model = load_weight(model, config['model_path'])

    # load image
    with open(config['image_path'], 'rb') as f:
        image_bytes = f.read()
    image = transform_image(image_bytes, config['image_size'])
    image = image.to(device)

    # predict the disease
    output = model(image)
    _, pred = torch.max(output, 1)
    pred_label = pred.item()
    pred_class = class_names[pred_label]
    print(pred_class)

    output[:, pred_label].backward()

    # get the activations of the last convolutional layer
    activations = model.get_activations(image).detach()

    # pull the gradients out of the model
    gradients = model.get_activations_gradient()

    # pool the gradients across the channels
    pooled_gradients = torch.mean(gradients, dim=[0, 2, 3])

    for i in range(pooled_gradients.shape[0]):
        activations[:, i, :, :] *= pooled_gradients[i]

    # average the channels of the activations
    heatmap = torch.mean(activations, dim=1).squeeze().cpu()

    # relu on top of the heatmap
    heatmap = np.maximum(heatmap, 0)

    # normalize the heatmap
    heatmap /= torch.max(heatmap)
    heatmap = heatmap.cpu().numpy()
    
    # get the curent folder of the picture
    inference_folder, picture_name = ntpath.split(config['image_path'])
    filename, file_ext = os.path.splitext(picture_name)

    filename_heatmap = filename + '_heatmap' + file_ext
    inference_heatmap = os.path.join(inference_folder, filename_heatmap)

    img = cv2.imread(config['image_path'])
    heatmap = cv2.resize(heatmap, (img.shape[1], img.shape[0]))
    heatmap = np.uint8(255 * heatmap)
    heatmap = cv2.applyColorMap(heatmap, cv2.COLORMAP_JET)
    superimposed_img = heatmap * 0.4 + img
    cv2.imwrite(inference_heatmap, superimposed_img)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Training and finetuning script for Chest X-RAY classification')

    parser.add_argument('--arch', default='resnet34', choices=['resnext50',
                                                                'resnet34',
                                                                'resnet50',
                                                                'mobilenetv2',
                                                                'vgg19',
                                                                'densenet121'],
                        help='Architecture (default: resnet34)')
    parser.add_argument('--model-path', default=None,
                        help='path of the photo for inference')
    parser.add_argument('--image-path', default=None,
                        help='path of the photo for inference')
    parser.add_argument('--imgsize', default=224, type=int,
                        help='Input image size')


    args = parser.parse_args()
    main(args)

# python inference.py --arch resnet50 --model-path "../best model/best.pth" --image-path ../inferences/00000502_005.png