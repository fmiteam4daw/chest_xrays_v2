import argparse
import json
import os
import time
import pdb

import pandas as pd
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torch.optim import lr_scheduler
from torch.utils.tensorboard import SummaryWriter

from dataset import load_class_names, prepare_loader

import sys
sys.path.insert(0, os.path.join('../'))
from models import construct_model

def get_lr(optimizer):
    for param_group in optimizer.param_groups:
        return param_group['lr']

def train_v1(ep, model, optimizer, #lr_scheduler, 
            train_loader, device, writer, weights):
    model.train()

    loss_meter = 0
    acc_meter = 0
    i = 0

    start_time = time.time()
    elapsed = 0

    for data, target in train_loader:
        data = data.to(device)
        target = target.to(device)

        optimizer.zero_grad()

        pred = model(data)

        # backprop the weighted loss/accuracy
        loss = F.cross_entropy(pred, target) #, weight=weights)
        loss.backward()
        optimizer.step()

        acc = pred.max(1)[1].eq(target).float().mean()

        loss_meter += loss.item()
        acc_meter += acc.item()
        i += 1
        elapsed = time.time() - start_time

        lr = get_lr(optimizer)

        print(f'Epoch {(ep + 1):03d} [{i}/{len(train_loader)}]: '
              f'Loss: {loss_meter / i:.4f} '
              f'Acc: {acc_meter / i:.4f} ({elapsed:.2f}s) '
              f'Learning Rate: {lr}', end='\r')

    # lr_scheduler.step()

    print()
    loss_meter /= len(train_loader)
    acc_meter /= len(train_loader)

    trainres = {
        'train_loss': loss_meter,
        'train_acc': acc_meter,
        'train_time': elapsed
    }

    # Record loss into the writer
    writer.add_scalar('Train/Loss', loss_meter, ep)
    writer.flush()

    return trainres

def test_v1(ep, model, test_loader, device, writer, weights):
    model.eval()

    loss_meter = 0
    acc_meter = 0
    runcount = 0
    elapsed = 0
    i = 0

    with torch.no_grad():
        start_time = time.time()
        for data, target in test_loader:
            data = data.to(device)
            target = target.to(device)

            pred = model(data)

            loss = F.cross_entropy(pred, target) * data.size(0) # , weight=weights
            acc = pred.max(1)[1].eq(target).float().sum()

            loss_meter += loss.item()
            acc_meter += acc.item()
            i += 1
            elapsed = time.time() - start_time
            runcount += data.size(0)

            print(f'[{i}/{len(test_loader)}]: '
                  f'Loss: {loss_meter / runcount:.4f} '
                  f'Acc: {acc_meter / runcount:.4f} ({elapsed:.2f}s)', end='\r')

        print()

        loss_meter /= runcount
        acc_meter /= runcount

    valres = {
        'val_loss': loss_meter,
        'val_acc': acc_meter,
        'val_time': elapsed
    }

    print(f'Test Result: Loss: {loss_meter:.4f} Acc: {acc_meter:.4f} ({elapsed:.2f}s)\n')

    # Record loss and accuracy into the writer
    writer.add_scalar('Test/Loss', loss_meter, ep)
    writer.add_scalar('Test/Accuracy', acc_meter, ep)
    writer.flush()

    return valres

def get_exp_dir(config):
    exp_dir = f'../logs/{config["arch"]}_{config["imgsize"][0]}_{config["epochs"]}'

    if config['finetune']:
        exp_dir += '_finetune'

    os.makedirs(exp_dir, exist_ok=True)

    exps = [d for d in os.listdir(exp_dir) if os.path.isdir(os.path.join(exp_dir, d))]
    files = set(map(int, exps))
    if len(files):
        exp_id = min(set(range(1, max(files) + 2)) - files)
    else:
        exp_id = 1

    exp_dir = os.path.join(exp_dir, str(exp_id))
    os.makedirs(exp_dir, exist_ok=True)

    json.dump(config, open(exp_dir + '/config.json', 'w'))

    return exp_dir

# init_dict = {}
# init_dict['linear1.weight'] = nn.init.xavier_uniform_(torch.empty((64, 1024)), gain=nn.init.calculate_gain('relu'))
# init_dict['linear1.bias'] = torch.empty(64)
# init_dict['linear2.weight'] = nn.init.xavier_uniform_(torch.empty((1024, 64)), gain=nn.init.calculate_gain('relu'))
# init_dict['linear2.bias'] = torch.empty(1024)
# sd['model'].update(init_dict)

def load_weight(model, optimizer, #lr_scheduler, 
                path):
    sd = torch.load(path, map_location=lambda storage, location: storage)

    model.load_state_dict(sd['model'])
    optimizer.load_state_dict(sd['optimizer'])
    # lr_scheduler.load_state_dict(sd['lr_scheduler'])
    epoch = sd['epoch']

    print('Loaded model from epoch %d\n' % (epoch))

    return epoch

def main(args):
    if torch.cuda.is_available():
        device = torch.device('cuda')
        torch.cuda.empty_cache()
    else:
        torch.device('cpu')

    config = {
        'batch_size': args.batch_size,
        'optimizer': args.optim,
        'lr': args.lr,
        'weight_decay': args.weight_decay,
        'momentum': args.momentum,
        'epochs': args.epochs,
        'imgsize': (args.imgsize, args.imgsize),
        'arch': args.arch,
        'finetune': args.finetune,
        'path': args.path,
        'version': '2'
    }

    exp_dir = get_exp_dir(config)

    class_names = load_class_names()
    num_classes = len(class_names)

    start_epoch = 0

    model = construct_model(config, num_classes)

    model = model.to(device)

    PATH_to_log_dir = '../runs/chest_xray/'
    # Declare Tensorboard writer
    timestr = time.strftime("%Y%m%d_%H%M%S")
    writer = SummaryWriter(PATH_to_log_dir + timestr)
    print('Tensorboard is recording into folder: ' + PATH_to_log_dir + timestr)


    # for child in model.base.children():
    #     for param in child.parameters():
    #         param.requires_grad = False

    # for child in model.linear1.children():
    #     for param in child.parameters():
    #         param.requires_grad = False

    # for child in model.linear2.children():
    #     for param in child.parameters():
    #         param.requires_grad = False

    optimizer = optim.SGD(model.parameters(),
                        lr=config['lr'],
                        momentum=config['momentum'],
                        weight_decay=config['weight_decay'])

    if config['optimizer'].lower == 'adam':
        optimizer = optim.Adam(model.parameters(), lr=config['lr'], weight_decay=config['weight_decay'])

    # lr_scheduler = optim.lr_scheduler.MultiStepLR(optimizer,
    #                                               [60, 120],
    #                                               gamma=0.1)
    # lr_scheduler = optim.lr_scheduler.StepLR(optimizer,
    #                                         40,
    #                                         gamma=0.1)

    if config['finetune']:
        start_epoch = load_weight(model, optimizer, #lr_scheduler, 
                                  config['path'])

    train_loader, test_loader, weights_per_class = prepare_loader(config)

    weights_per_class = {x: weights_per_class[x].to(device)
                            for x in ['train', 'test']}

    best_acc = 0
    res = []

    train = train_v1
    test = test_v1

    start_epoch = 0

    for ep in range(start_epoch, config['epochs']):
        trainres = train(ep=ep, model=model, optimizer=optimizer, train_loader=train_loader, #lr_scheduler=lr_scheduler, 
                            device=device, writer=writer, weights=weights_per_class['train'])
        valres = test(ep, model, test_loader, device, writer, weights_per_class['test'])
        trainres.update(valres)

        if best_acc < valres['val_acc']:
            best_acc = valres['val_acc']

            torch.save({
                'model': model.state_dict(),
                'optimizer': optimizer.state_dict(),
                # 'lr_scheduler': lr_scheduler.state_dict(),
                'epoch': ep},
                os.path.join(exp_dir, 'best.pth'))

        res.append(trainres)

    print(f'Best accuracy: {best_acc:.4f}')
    res = pd.DataFrame(res)
    res.to_csv(exp_dir + '/history.csv')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Training and finetuning script for Chest X-RAY classification')

    # training arg
    parser.add_argument('--batch-size', default=32, type=int,
                        help='training batch size (default: 32)')

    parser.add_argument('--epochs', default=40, type=int,
                        help='training epochs (default: 40)')
    parser.add_argument('--arch', default='resnet34', choices=['resnext50',
                                                                'resnet34',
                                                                'mobilenetv2',
                                                                'vgg19',
                                                                'densenet121'],
                        help='Architecture (default: resnet34)')
    parser.add_argument('--imgsize', default=224, type=int,
                        help='Input image size')
    parser.add_argument('--finetune', default=False, action='store_true',
                        help='whether to finetune from 400x400 to 224x224 (default: False)')
    parser.add_argument('--path', default=None,
                        help='required if it is a finetune task (default: None)')


    # optimizer arg
    parser.add_argument('--optim', default='SGD', type=str,
                        help='Optimizer (default: SGD)')
    parser.add_argument('--lr', default=0.01, type=float,
                        help='Optimizer learning rate (default: 0.01)')
    parser.add_argument('--weight-decay', default=1e-4, type=float,
                        help='Optimizer weight decay (default: 0.0001)')
    parser.add_argument('--momentum', default=0.9, type=float,
                        help='SGD momentum (default: 0.9)')

    args = parser.parse_args()

    if args.finetune and args.path is None:
        parser.error('--finetune requires --path')

    main(args)

# python train_se.py --arch resnext50 --epochs 40 --optim adam --batch-size 25 --finetune --path ../logs/resnext50_224_40_finetune/5/best.pth
# --finetune --path ../logs/densenet121_224_30_finetune/1/best.pth