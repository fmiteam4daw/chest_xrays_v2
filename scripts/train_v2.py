import argparse
import json
import os
import time

import pandas as pd
import torch
import torch.optim as optim
import torch.nn as nn
from torch.optim import lr_scheduler
from torch.utils.tensorboard import SummaryWriter

from dataset import prepare_loader

import sys
sys.path.insert(0, os.path.join('../'))
from models import construct_model

def get_exp_dir(config):
    exp_dir = f'../logs/a_no_finding/{config["arch"]}_{config["imgsize"][0]}_{config["epochs"]}'

    if config['finetune']:
        exp_dir += '_finetune'

    os.makedirs(exp_dir, exist_ok=True)

    exps = [d for d in os.listdir(exp_dir) if os.path.isdir(os.path.join(exp_dir, d))]
    files = set(map(int, exps))
    if len(files):
        exp_id = min(set(range(1, max(files) + 2)) - files)
    else:
        exp_id = 1

    exp_dir = os.path.join(exp_dir, str(exp_id))
    os.makedirs(exp_dir, exist_ok=True)

    json.dump(config, open(exp_dir + '/config.json', 'w'))

    return exp_dir

def load_weight(model, optimizer, lr_scheduler, path):
    sd = torch.load(path)
    model.load_state_dict(sd['model'])
    optimizer.load_state_dict(sd['optimizer'])
    lr_scheduler.load_state_dict(sd['lr_scheduler'])
    epoch = sd['epoch']

    print('Loaded model from epoch %d\n' % (epoch + 1))

    return epoch

def get_lr(optimizer):
    for param_group in optimizer.param_groups:
        return param_group['lr']

def train_model(model, dataloaders, weights, criterion, optimizer, lr_scheduler, num_epochs, exp_dir, device, writer):
    since = time.time()

    best_acc = 0.0

    res = []

    for epoch in range(num_epochs):
        print('Epoch {}/{}'.format(epoch + 1, num_epochs))
        print('-' * 10)

        # Each epoch has a training and validation phase
        for phase in ['train', 'test']:
            if phase == 'train':
                model.train()  # Set model to training mode
            else:
                model.eval()   # Set model to evaluate mode

            running_loss = 0.0
            running_corrects = 0
            runcount = 0
            i = 0

            # Iterate over data.
            for inputs, labels in dataloaders[phase]:
                inputs = inputs.to(device)
                labels = labels.to(device)

                # zero the parameter gradients
                optimizer.zero_grad()

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):
                    # Get model outputs and calculate loss
                    
                    outputs = model(inputs)
                    loss = criterion(outputs, labels)

                    _, preds = torch.max(outputs, 1)

                    # backward + optimize only if in training phase
                    if phase == 'train':
                        loss.backward()
                        optimizer.step()

                # statistics
                running_loss += loss.item() * inputs.size(0)
                running_corrects += torch.sum(preds == labels.data)

                lr = get_lr(optimizer)
                batch_size = inputs.size(0)
                runcount += batch_size
                i += 1

                print(f'{phase} [{i}/{len(dataloaders[phase])}]: '
                    f'Loss: {running_loss / runcount:.4f} '
                    f'Acc: {(running_corrects.double() / runcount * 100):.2f}% '
                    f'Learning Rate: {lr}', end='\r')
                       
            print()

            epoch_loss = running_loss / len(dataloaders[phase].dataset)
            epoch_acc = running_corrects.double() / len(dataloaders[phase].dataset)

            if phase == 'test' and epoch_acc > best_acc:
                best_acc = epoch_acc
                
                torch.save({
                    'model': model.state_dict(),
                    'optimizer': optimizer.state_dict(),
                    'lr_scheduler': lr_scheduler.state_dict(),
                    'epoch': epoch},
                    os.path.join(exp_dir, 'best.pth'))
                
            if phase == 'train':
                trainres = {
                    'train_loss': epoch_loss,
                    'train_acc': epoch_acc.item(),
                }

                # Record loss / acc into the writer
                writer.add_scalar('Train/Loss', epoch_loss, epoch)
                writer.add_scalar('Train/Accuracy', epoch_acc, epoch)
                writer.flush()

            if phase == 'test':
                valres = {
                    'val_loss': epoch_loss,
                    'val_acc': epoch_acc.item(),
                }

                trainres.update(valres)
                res.append(trainres)

                # Record loss / acc into the writer
                writer.add_scalar('Test/Loss', epoch_loss, epoch)
                writer.add_scalar('Test/Accuracy', epoch_acc, epoch)
                writer.flush()

                try:
                    lr_scheduler.step()
                except:
                    lr_scheduler.step(epoch_loss)

        print()

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))
    print('Best val Acc: {:4f}'.format(best_acc))

    res = pd.DataFrame(res)
    res.to_csv(exp_dir + '/history.csv')

    return model

def main(args):
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

    torch.cuda.empty_cache()

    config = {
        'batch_size': args.batch_size,
        'optimizer': args.optim,
        'lr': args.lr,
        'weight_decay': args.weight_decay,
        'momentum': args.momentum,
        'epochs': args.epochs,
        'imgsize': (args.imgsize, args.imgsize),
        'arch': args.arch,
        'finetune': args.finetune,
        'path': args.path,
        # 'feature_extract': args.feature_extract,
        'version': '1'
    }

    train_loader, test_loader, weights_per_class = prepare_loader(config)
    dataloaders_dict = {'train': train_loader, 'test': test_loader}

    weights_per_class = {x: weights_per_class[x].to(device)
                            for x in ['train', 'test']}

    class_names = train_loader.dataset.classes
    num_classes = len(class_names)

    exp_dir = get_exp_dir(config)

    # Initialize the model for this run
    # model = initialize_model(config['arch'], num_classes, config['feature_extract'], use_pretrained=True)
    model = construct_model(config, num_classes)

    model = model.to(device)

    # Gather the parameters to be optimized/updated in this run. If we are
    #  finetuning we will be updating all parameters. However, if we are
    #  doing feature extract method, we will only update the parameters
    #  that we have just initialized, i.e. the parameters with requires_grad
    #  is True.

    params_to_update = model.parameters()
    # print("Params to learn:")
    # if config['feature_extract']:
    #     params_to_update = []
    #     for name,param in model.named_parameters():
    #         if param.requires_grad == True:
    #             params_to_update.append(param)
    #             print("\t",name)
    # else:
    #     for name,param in model.named_parameters():
    #         if param.requires_grad == True:
    #             print("\t",name)

    PATH_to_log_dir = exp_dir + '/runs/'
    # Declare Tensorboard writer
    writer = SummaryWriter(PATH_to_log_dir)
    print('Tensorboard is recording into folder: ' + PATH_to_log_dir + '\n')

    optimizer_ft = optim.SGD(params_to_update,
                        lr=config['lr'],
                        momentum=config['momentum'],
                        weight_decay=config['weight_decay'])
    if config['optimizer'].lower == 'adam':
        optimizer_ft = optim.Adam(params_to_update,
                                      lr=config['lr'],
                                      weight_decay=config['weight_decay'])

    lr_scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer_ft,
                                                        factor = 0.1,
                                                        patience = 5,
                                                        mode = 'min')

    if config['finetune']:
        load_weight(model, optimizer_ft, lr_scheduler, config['path'])

    # Setup the loss fxn
    criterion = nn.CrossEntropyLoss()

    # Train and evaluate
    model = train_model(model, dataloaders_dict, weights_per_class, criterion, optimizer_ft, lr_scheduler, config['epochs'], exp_dir, device, writer)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Training and finetuning script for Chest X-RAY classification')

    # training arg
    parser.add_argument('--batch-size', default=32, type=int,
                        help='training batch size (default: 32)')
    parser.add_argument('--epochs', default=40, type=int,
                        help='training epochs (default: 40)')
    parser.add_argument('--arch', default='resnet34', choices=['resnext50',
                                                                'resnet34',
                                                                'resnet50',
                                                                'mobilenetv2',
                                                                'vgg19',
                                                                'densenet121'],
                        help='Architecture (default: resnet34)')
    parser.add_argument('--imgsize', default=224, type=int,
                        help='Input image size')
    parser.add_argument('--finetune', default=False, action='store_true',
                        help='whether to finetune from 400x400 to 224x224 (default: False)')
    parser.add_argument('--path', default=None,
                        help='required if it is a finetune task (default: None)')

    # optimizer arg
    parser.add_argument('--optim', default='SGD', type=str,
                        help='Optimizer (default: SGD)')
    parser.add_argument('--lr', default=0.01, type=float,
                        help='Optimizer learning rate (default: 0.01)')
    parser.add_argument('--weight-decay', default=1e-4, type=float,
                        help='Optimizer weight decay (default: 0.0001)')
    parser.add_argument('--momentum', default=0.9, type=float,
                        help='SGD momentum (default: 0.9)')

    args = parser.parse_args()

    if args.finetune and args.path is None:
        parser.error('--finetune requires --path')

    main(args)
