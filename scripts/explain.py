import argparse
import json
import os
import time
import cv2

import pandas as pd
import numpy as np
import torch
import pdb

import sys
sys.path.insert(0, os.path.join('../'))
from dataset import load_class_names, prepare_loader
from models import construct_model
from test import test_v1

import matplotlib.pyplot as plt

def load_weight(model, path):
    sd = torch.load(path)
    model.load_state_dict(sd)
    return model


def get_exp_dir(config):
    exp_dir = f'../logs/{config["arch"]}_{config["imgsize"][0]}_{config["epochs"]}'

    if config['finetune']:
        exp_dir += '_finetune'

    exp_id = config['run_number']
    exp_dir = os.path.join(exp_dir, str(exp_id))

    return exp_dir


def main(args):
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

    config = {
        'batch_size': args.batch_size,
        'epochs': args.epochs,
        'imgsize': (args.imgsize, args.imgsize),
        'arch': args.arch,
        'finetune': args.finetune,
        'path': args.path,
        'run_number': args.run_number,
        'picture_index': args.picture_index,
        'version': '1CAM'
    }

    exp_dir = get_exp_dir(config)
    config['path'] = os.path.join(exp_dir, 'best.pth')

    class_names = load_class_names()
    num_classes = len(class_names)

    model = construct_model(config, num_classes)
    model = load_weight(model=model, path=config['path'])

    model = model.to(device)
    # model.base.layer2[0].conv1
    # model.base.layer2[0].conv1.weight # see weights
    # model.base.layer2[0].conv1.weight.requires_grad = False # freezing
    # model.base.layer2[0].bn1.bias.requires_grad = False # freezing
    # model.base.layer4[-1].relu
    idx = config['picture_index']

    train_loader, test_loader, _ = prepare_loader(config)
    iter_loader = iter(test_loader)
    images, labels = next(iter_loader)
    img = images[idx].unsqueeze(0)
    img = img.to(device)

    label = labels[idx]
    pred = model(img)
    pred_label = pred.argmax(dim=1).item()
    
    pred[:, pred_label].backward()

    # get the activations of the last convolutional layer
    activations = model.get_activations(img).detach()

    # pull the gradients out of the model
    gradients = model.get_activations_gradient()

    # pool the gradients across the channels
    pooled_gradients = torch.mean(gradients, dim=[0, 2, 3])

    for i in range(pooled_gradients.shape[0]):
        activations[:, i, :, :] *= pooled_gradients[i]

    # average the channels of the activations
    heatmap = torch.mean(activations, dim=1).squeeze().cpu()

    # relu on top of the heatmap
    heatmap = np.maximum(heatmap, 0)

    # normalize the heatmap
    heatmap /= torch.max(heatmap)
    heatmap = heatmap.cpu().numpy()

    # draw the heatmap
    plt.matshow(heatmap.squeeze())
    plt.savefig('fig.png')


    img = cv2.imread('../filtered/test/Atelectasis/00000086_002.png')
    heatmap = cv2.resize(heatmap, (img.shape[1], img.shape[0]))
    heatmap = np.uint8(255 * heatmap)
    heatmap = cv2.applyColorMap(heatmap, cv2.COLORMAP_JET)
    superimposed_img = heatmap * 0.4 + img
    cv2.imwrite('map.png', superimposed_img)

    return None



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Explain a trained model')

    parser.add_argument('--batch-size', default=32, type=int,
                        help='training batch size (default: 32)')
    parser.add_argument('--epochs', default=40, type=int,
                        help='training epochs (default: 40)')
    parser.add_argument('--arch', default='resnet34', choices=['resnext50',
                                                                'resnet34',
                                                                'mobilenetv2'],
                        help='Architecture (default: resnet34)')
    parser.add_argument('--imgsize', default=224, type=int,
                        help='Input image size')
    parser.add_argument('--finetune', default=False, action='store_true',
                        help='whether to finetune from 400x400 to 224x224 (default: False)')
    parser.add_argument('--path', default=None,
                        help='required if it is a finetune task (default: None)')
    parser.add_argument('--run-number', default=1,
                        help='run number of the model to be explained')
    parser.add_argument('--picture-index', default=0, type=int,
                        help='the index of the picture to be activation mapped')

    args = parser.parse_args()
    main(args)

# python explain.py --arch resnext50 --batch-size 25 --run-number 3 --picture-index 3